import QtQuick 2.13
import QtQuick.Window 2.13
import QtMultimedia 5.12
import QtQuick.Controls 2.12

Window {
    id: window
    visible: true
    width: 640
    height: 480

    visibility: Window.FullScreen
    title: qsTr("Camera module")

    Item {
        id: root
        property bool isRecorder: false; //Boolean to determine if PictureMode or RecordingMode is on.
        property string lastImageCapturePath: ""
        anchors.fill: parent

        signal fatalError

        Camera {
            id: camera
            position: Camera.BackFace
            imageProcessing.whiteBalanceMode: CameraImageProcessing.WhiteBalanceFlash
            onError: { //Error report
                if (Camera.NoError !== error) {
                    console.log("Error report: " + error + " errorString " + errorString)
                    root.fatalError() //Signal
                }
            }
            exposure {
                exposureCompensation: -1.0
                exposureMode: Camera.ExposurePortrait
            }
            flash.mode: Camera.FlashOn
            focus {} //Optional: Insert camera focus options here.

            imageCapture { // Accessing can be done with "camera.imageCapture"
                onImageCaptured: {
//                    photoPreview.source = preview  // Show the preview in an Image
                    imageCaptureInfoText.text = "Image captured";
                    root.lastImageCapturePath = camera.imageCapture.capturedImagePath;
                    console.log("Captured image path: " + camera.imageCapture.capturedImagePath);
                }
            }
        }
        VideoOutput { // The screen that shows livefeed from camera
            id: viewfinder
            source: camera
            autoOrientation: true
            anchors.fill: parent
            focus : visible // To receive focus and capture key events when visible

            MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            camera.searchAndLock(); //!! Focusing camera | Not working on Qt 5.4.0 beta
                            imageCaptureInfoText.text = "";
                        }
                        onDoubleClicked: {
                            if(root.isRecorder){ //If RecordingMode is on. Check if it's recording or not.
                                if(camera.videoRecorder.recorderState == 0){ //Check if the recording is currenlty on. 0 = off | 1 = on
                                    camera.flash.mode = Camera.FlashOn;
                                    camera.captureMode = Camera.CaptureVideo;
                                    camera.videoRecorder.record();
                                }
                                else{
                                    camera.videoRecorder.stop();
                                }
                            }
                            else{ //If the RecordingMode is off, it means PictureMode is on, prepare camera and shoot.
                                camera.captureMode = Camera.CaptureStillImage;
                                camera.imageCapture.capture();
                                }
                            }
                    }
            // Display focus areas on camera viewfinder:
            Repeater { // The repeater is used for updating the focus info.
                  model: camera.focus.focusZones
                  Rectangle {
                      border {
                          width: 2
                          color: status == Camera.FocusAreaFocused ? "green" : "red"
                      }
                      color: "transparent"

                      // Map from the relative, normalized frame coordinates
                      property variant mappedRect: viewfinder.mapNormalizedRectToItem(area);

                      x: mappedRect.x
                      y: mappedRect.y
                      width: mappedRect.width
                      height: mappedRect.height
                }
            }
        }

        Image { //Captured phots are temporarly stored here.
            id: photoPreview
        }

        TabBar {
            id: tabBar
            width: root.width
            anchors.top: root.top

            TabButton {
                text: root.isRecorder ? "Video" : "Photo"
                onClicked: {
                    root.isRecorder = !root.isRecorder
                }
            }
            TabButton {
                text: "Swap camera"
                checkable: false
                onClicked: {
                    camera.position = camera.position == Camera.BackFace ? Camera.FrontFace : Camera.BackFace
                }
            }
        }

        Text { // Text for notifying the user that the recorder is recording currently
            id: recordingLabel
            anchors.top: tabBar.bottom
            width: root.width
            height: 50

            text: camera.videoRecorder.recorderState == 0 ? "" : "Recording"
            color: "red"
        }

        Text{ // Text for notifying the user that the picture has been successfully taken
            id: imageCaptureInfoText
            anchors.bottom: flashBtn.top
            horizontalAlignment: Qt.AlignCenter
            width: root.width
            height: 50

            text: ""
            color: text == "Image captured" ? "green" : "red"
        }

        Button{
            //This is not the best solution for flashlight testing/ IF the camera doesn't work there might be a possibillity that the flash won't work either.
            id: flashBtn
            height: 50
            anchors.bottom: root.bottom
            width: root.width
            text: "Flash test"
            onClicked: {
                if(camera.flash.mode !== Camera.FlashOn){
                    camera.flash.mode = Camera.FlashOn
                }
                camera.searchAndLock()
            }
        }
    }
}
